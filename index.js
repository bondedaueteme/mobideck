function validateForm() {
			usernamePattern = /^[A-Za-z][A-Za-z0-9_]{1,29}$/;
			passwordPattern = /^[A-Za-z0-9!@#$%^&*()_]{6,20}$/;
			
    		var username = $("#username_box").val();
			var password = $("#password_box").val();
    		var errorMessage ="";
			var returnValue;
			if(!usernamePattern.test(username))
			{
				errorMessage = errorMessage + "Username must have at least 2 characters, start with an alphabet and contains only alphabets, numbers or underscore \n";
			}
			if(!passwordPattern.test(password))
			{
				errorMessage = errorMessage +"Password must be at least 6 characters long and can contain only alphabets, numbers and these symbols: \"!@#$%^&*()_\"";
			}
			if (errorMessage!="") {
        		alert(errorMessage);
				return false;
    		}
			else
			{
				return true;
			}
			
}
function login()
{
	if (validateForm())
	{
		var username = $("#username_box").val();
		var password = $("#password_box").val();
		$.post("validateLogin.php", {password: password, username:username}, function(data){
			if($.trim(data)=="login_success")
			{
			location.href = "mainPage.php";
			}
			else
			{
				$('#login_status').html(data);
			}
    });
	}
}