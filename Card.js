var Card = function(img, flip, pos, width, height){
	//this.source = img_source;
	
	this.image = img;
	this.naturalImage = img;
	this.flipImage = flip;
	this.pos = Coord.copy(pos);
	this.width = width;
	this.height = height;
	this.angle = 0;
	this.show = false;
	this.depth = 0;
}

Card.prototype.draw = function(context){
		
	var canvas = document.getElementById("gameCanvas");
	var context = canvas.getContext("2d");
	
	x = this.pos.x;
	y = this.pos.y;
	
	image = new Image();
	image = this.image;
	context.drawImage(image, x, y, this.width, this.height);
	
}

Card.prototype.flip = function(){
	// if card is flipped open
	
	var canvas = document.getElementById("gameCanvas");
	var ctx = canvas.getContext("2d");
	
	
	
	if(this.image==this.naturalImage){
		this.image = this.flipImage

		this.draw();
	}
	else {
		this.image = this.naturalImage;
		this.draw();
	}
}

Card.prototype.containsPos = function(a,b) {

	var x = []
	var y = []
	x[0] = this.pos.x;
	y[0] = this.pos.y;
	x[1] = Math.cos(this.angle)*this.width + x[0];
	y[1] = Math.sin(this.angle)*this.width + y[0];
	x[3] = Math.cos(this.angle + Math.PI/2)*this.height + x[0];
	y[3] = Math.sin(this.angle + Math.PI/2)*this.height + y[0];
	x[2] = Math.cos(this.angle)*this.width + x[3];
	y[2] = Math.sin(this.angle)*this.width + y[3];

	for (i = 0; i < 4; i++) {

		var x1 = x[i];
		var y1 = y[i];
		var x2 = x[(i+1)%4];
		var y2 = y[(i+1)%4];

		D = ((x2 - x1) * (b - y1) - (y2 - y1) * (a - x1));
		
		if (D < 0) {
			return false;
		}
	}
	return true;
}