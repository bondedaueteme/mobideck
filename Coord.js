var Coord = {};

Coord.distance = function(point1, point2) {

	var d1 = Math.abs(point2.x - point1.x);
	var d2 = Math.abs(point2.y - point1.y);

	return Math.sqrt(d1*d1 + d2*d2);
}

Coord.angle = function(point1, point2, point3) {

	if (point3) {
		angle1 = Math.atan2((point2.y - point1.y), (point2.x - point1.x));
		angle2 = Math.atan2((point3.y - point1.y), (point3.x - point1.x));

		return angle2 - angle1;
	} else {
		return Math.atan2((point2.y - point1.y), (point2.x - point1.x));
	}
}

Coord.copy = function(point1, point2) {
	
	if (point2) {
		point1.x = point2.x;
		point1.y = point2.y;
	} else {
		return {x:point1.x, y:point1.y};
	}
}