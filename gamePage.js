function relMouseCoords(event) {
    var totalOffsetX = 0;
    var totalOffsetY = 0;
    var canvasX = 0;
    var canvasY = 0;
    var currentElement = this;

    do {
        totalOffsetX += currentElement.offsetLeft;
        totalOffsetY += currentElement.offsetTop;
    }
    while (currentElement = currentElement.offsetParent)

    canvasX = event.pageX - totalOffsetX;
    canvasY = event.pageY - totalOffsetY;

    // Fix for variable canvas width
    canvasX = Math.round( canvasX * (this.width / this.offsetWidth) );
    canvasY = Math.round( canvasY * (this.height / this.offsetHeight) );

    return {x:canvasX, y:canvasY}
}
HTMLCanvasElement.prototype.relMouseCoords = relMouseCoords;

function manualShuffle(deck, e) {
	var canvas = document.getElementById("gameCanvas");
	coords = canvas.relMouseCoords(e);
	x = coords.x;
	y = coords.y;
	aux1 = deck.orderList[0].pos.x + deck.orderList[0].width;
	aux2 = deck.orderList[0].pos.y + deck.orderList[0].height;
		console.log("touch: "+ x + ", " + y);
		console.log("interval x: " + deck.orderList[0].pos.x + " ," + aux1);
		console.log("interval y: " + deck.orderList[0].pos.y + " ," + aux2);
	if(deck.orderList[0].containsPos(x,y)) {
		deck.shuffleAnimated();
		canvas.drawCards();
	}
}

function changeBack(carta, e) {
	if(carta.containsPos(e.touches[0].pageX,e.touches[0].pageY)){
		carta.flip();
	}
}

window.requestAnimFrame = (function(callback) {
return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame ||
function(callback) {
  window.setTimeout(callback, 1000 / 60);
};
})();
	  
function getDistance(x1,y1,x2,y2)
{
	return Math.abs(y2-y1) + Math.abs(x2-x1);
}

function animateO(card, canvas, context, startTime, finalPosition) {

		var angle = Coord.angle(card.pos, finalPosition);
		
        var time = (new Date()).getTime() - startTime;

        var linearSpeed = 5;

        var newPos = {x:card.pos.x + linearSpeed * time * Math.cos(angle) / 1000,
        y:card.pos.y + linearSpeed * time * Math.sin(angle) / 1000};

        var currentDistance = Coord.distance(card.pos, finalPosition);
		var newDistance = Coord.distance(newPos, finalPosition);

		if (newDistance <= currentDistance)
		{
			Coord.copy(card.pos, newPos);
			canvas.drawCards();
        }
		else
		{
			canvas.drawCards();
			return;
		}

        // request new frame
        requestAnimFrame(function() {
          animateO(card, canvas, context, startTime,finalPosition);
        });
}

$(document).ready(
	function(){
		
		var canvas = document.getElementById("gameCanvas");
		
		canvas.width = screen.width;
		canvas.height = screen.height;

		gameImages = loadImages();
		
		canvas.addEventListener("allImagesLoaded", 
		function() { 
			gameData = loadGameData(gameImages);
			gameStart(gameData); 
		}
		, false);
		
	}
);

function getMiddle(cardWidth,cardHeight,canvas)
{
	xposition = (canvas.width - cardWidth)/2;
	yposition = (canvas.height - cardHeight)/2;
	return {x:xposition, y:yposition};
}

function getAngle(initialX,initialY,finalX,finalY)
{
	var angle = Math.atan2((finalY-initialY),(finalX-initialX));
	return angle;
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getCardImage(cardsMatrix)
{
	var suit = getRandomInt(0,3);
	var value = getRandomInt(0,12);
	return cardsMatrix[value][suit];
	
}

 function gameStart(gameData) {

	var flipImg = gameData.miscImages[0];
	var cardWidth = 70;
	var cardHeight = 100;
	var canvas = document.getElementById("gameCanvas");
	canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    canvas.style.backgroundColor = '#32CD32';
    var context = canvas.getContext('2d');
	
	var deck = new cardDeck({x:300, y:100}, gameData.cards.allCards);
	deck.randomShuffle();
	
	canvas.addEventListener("mousedown", function(e){ manualShuffle(deck,e) }, false);

	canvas.drawCards = function() {
		console.log("draw function called");
		context.clearRect(0, 0, canvas.width, canvas.height);
		orderedCards = [];
		for (card of gameData.cards.allCards) { orderedCards.push(card); }
		orderedCards.sort(function(a, b) { return b.depth - a.depth });
		for (card of orderedCards) { if (card.show) {card.draw(context); } }
	}

	canvas.drawCards();
	
	canvas.style.backgroundColor = '#32CD32';
     
	var middlePosition = getMiddle(cardWidth,cardHeight,canvas);

	var dealtCardsDepth = 0;

	$('#player1').on('click', function(){
		card = deck.drawTop();
		card.depth = dealtCardsDepth--;
		makeAMove(1, middlePosition, card, canvas, context, cardWidth, cardHeight);
	});
	$('#player2').on('click', function(){
		card = deck.drawTop();
		card.depth = dealtCardsDepth--;
	 	makeAMove(2, middlePosition, card, canvas, context, cardWidth, cardHeight);
	});
	$('#player3').on('click', function(){
		card = deck.drawTop();
		card.depth = dealtCardsDepth--;
		makeAMove(3, middlePosition, card, canvas, context, cardWidth, cardHeight);
	});
	$('#player4').on('click', function(){
		card = deck.drawTop();
		card.depth = dealtCardsDepth--;
		makeAMove(4, middlePosition, card, canvas, context, cardWidth, cardHeight);
	});

  	var flipImg = new Image();
	flipImg.src = 'card_sprites/PNG-cards-1.3/flip.png';
}

function makeAMove(playerNumber, middlePosition, card, canvas, context, cardWidth, cardHeight)
{
	var initialPosition = Coord.copy(middlePosition);

	switch (playerNumber) { 
		case 1:
			initialPosition.y = -1.0 * cardHeight;
			break;
		case 2:
			initialPosition.x = -1.0 * cardWidth;
			break;
		case 3:
			initialPosition.y = canvas.height;
			break;
		case 4:
			initialPosition.x = canvas.width;
			break;
	}
	
	var startTime = (new Date()).getTime();

	card.pos = initialPosition;
	card.width = cardWidth;
	card.height = cardHeight;

    animateO(card, canvas, context, startTime, middlePosition);
	
}
