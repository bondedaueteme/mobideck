var Card = function(img, flip){
	//this.source = img_source;
	
	this.image= img;
	this.naturalImage = img;
	this.flipImage = flip;
	this.xPos = 0;
	this.yPos = 0;
}

Card.prototype.draw = function(){
		
	card_width = 50;
	card_height = 50;
	var canvas = document.getElementById("gameCanvas");
	var context = canvas.getContext("2d");
	
	x = this.xPos;
	y = this.yPos;
	image = new Image();
	image = this.image;
	
		context.clearRect(0,0,canvas.width,canvas.height);
		//context.drawImage(base_image,x,y,30,42);
		// eu tentando recortar de uma img grande
		context.drawImage(image,x,y,200,290);
}

Card.prototype.flip = function(){
	// if card is flipped open
	
	var canvas = document.getElementById("gameCanvas");
	var ctx = canvas.getContext("2d");
	
	if(this.image==this.naturalImage){
		this.image = this.flipImage

		this.draw();
	}
	else {
		this.image = this.naturalImage;
		this.draw();
	}
}

function changeBack(carta){
	carta.flip();
	console.log("inn");
}


$(document).ready(
	function(){
		
		var canvas = document.getElementById("gameCanvas");
		canvas.width = screen.width;
		canvas.height = screen.height;
		testLoad();
		
		canvas.addEventListener("allImagesLoaded", gameStart, false);
		
		
	}
);

function gameStart(){
		flipImg = imagesArray[0];
	
		var canvas = document.getElementById("gameCanvas");
		var carta = new Card(cardsMatrix[1][1],flipImg);
			
		carta.draw();
		
		canvas.addEventListener("touchstart", function(){ changeBack(carta) }, false);
		
		canvas.addEventListener("touchend", function(){ changeBack(carta) }, false);
		
		canvas.style.backgroundColor = '#32CD32';
}