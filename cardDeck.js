function swap(a, b) {
	var aux = a.value;
	a.value = b.value;
	b.value = aux;
}

var cardDeck = function(position, cardsList) {

	this.orderList = [];
	for (card of cardsList) { this.orderList.push(card); }
	this.orderList.sort(function(a, b) { return a.depth - b.depth });
	this.pos = Coord.copy(position);	
}

cardDeck.prototype.organize = function() {

	var cardWidth = 140;
    var cardHeight = 200;

    this.orderList.sort(function(a, b) { return a.depth - b.depth });

	for (i = this.orderList.length-1; i >= 0; i--) {

		card = this.orderList[i];
		variationY = i / 10;
		variationX = i / 3;
		card.pos.x = this.pos.x + variationX;
		card.pos.y = this.pos.y + variationY;
		card.width = cardWidth;
		card.height = cardHeight;
		card.show = true;
	}
	
	return this.orderList;
}

cardDeck.prototype.randomShuffle = function() {
	possibleDepths = [];
	for (i = 0; i < this.orderList.length; i++) {
		possibleDepths.push(i);
	}
	while (possibleDepths.length) {
		var i = possibleDepths.length - 1;
		r = random(0, i);
		this.orderList[i].depth = possibleDepths[r];
		possibleDepths.splice(r, 1);
	}
	this.organize();
}

function random(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

cardDeck.prototype.shuffleAnimated = function() {

	var shuffleTimes = 1;
	for(i = 0; i < shuffleTimes; i++){
		var card1 = random(0, this.orderList.length-1);
		this.swapAnimated(card1);
	}
}

cardDeck.prototype.swapAnimated = function(c2) {

	var card1 = this.orderList[0];
	var card2 = this.orderList[c2];

	var aux = card1.pos;
	card1.pos = card2.pos;
	card2.pos = aux;

	aux = card1.depth;
	card1.depth = card2.depth;
	card2.depth = aux;

	this.orderList[0] = card2;
	this.orderList[c2] = card1;
}

cardDeck.prototype.drawTop = function() {

	if (this.orderList.length) {
		var topCard = this.orderList[0];
		this.orderList.shift();
		return topCard;
	}
}