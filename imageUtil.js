function loadCards() {
	var card_names = ["2","3","4","5","6","7","8","9","10","jack","queen","king","ace"];
	var card_suits = ["clubs","diamonds","hearts","spades"];
	var cardsMatrix = new Array(13);
	for(i = 0; i < 13; i++){
		cardsMatrix[i] = new Array(4);
		for(j = 0; j < 4; j++){
			card_name = "card_sprites/PNG-cards-1.3/"+card_names[i]+"_of_"+card_suits[j]+".png";
			cardsMatrix[i][j] = new Image();
			cardsMatrix[i][j].src = card_name;
		}
	}
	return cardsMatrix;
}


function loadGeneralImages() {
	var array = new Array(1);
	
	array[0] = new Image();
	array[0].src = 'card_sprites/PNG-cards-1.3/flip.png';
	
	return array;
}

function loadImages() {

	gameImages = {};

	gameImages.cardImgMatrix = loadCards();
	gameImages.miscImages = loadGeneralImages();

	var quantImages = 53;
	
	var event = new CustomEvent("allImagesLoaded", {
			detail: {
				time: new Date(),
			},
			bubbles: true,
			cancelable: true
	});
	
	var quant = 1;
	for(i = 0;i < 13; i++) {
		for(j = 0; j < 4; j++) {
			gameImages.cardImgMatrix[i][j].onload = function() {
				quant++;
				if(quant == quantImages){
					document.getElementById("gameCanvas").dispatchEvent(event);
				}
			};
		}
	}
	
	for(i = 0; i < gameImages.miscImages.length; i++) {
		gameImages.miscImages[i].onload = function() {
			quant++;
			if(quant == quantImages) {
				document.getElementById("gameCanvas").dispatchEvent(event);
			}
		};
	}

	return gameImages;
}

function loadGameData(gameImages) {

	gameData = {};

	gameData.cardImgMatrix = gameImages.cardImgMatrix;
	gameData.miscImages = gameImages.miscImages;
	gameData.cards = {allCards:[]};

	var card_names = ["2","3","4","5","6","7","8","9","10","jack","queen","king","ace"];
	var card_suits = ["clubs","diamonds","hearts","spades"];

	for(i = 0; i < 13; i++) {

		for(j = 0; j < 4; j++) {

			var value = card_names[i];
			var suit = card_suits[j];

			if (gameData.cards[value] == undefined) {
				gameData.cards[value] = [];
			}

			if (gameData.cards[suit] == undefined) {
				gameData.cards[suit] = [];
			}
			var card = new Card(gameData.cardImgMatrix[i][j], gameData.miscImages[0], {x:0, y:0}, 140, 200);
			gameData.cards[value].push(card);
			gameData.cards[suit].push(card);
			gameData.cards.allCards.push(card);
		}
	}

	return gameData;
} 